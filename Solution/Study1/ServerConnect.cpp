#include "ServerConnect.h"
#include <omp.h>  


using namespace std;
#define memequ(a,b,len) (memcmp(a,b,len) == 0)

ServerConnect::ServerConnect(ServerInfo**servers, int serversCount) :
   servers(servers), serversCount(serversCount),
   selectedServer(NULL),
   apiFactory("mtmanapi.dll"), api(NULL)
{
   apiFactory.WinsockStartup();
   if (apiFactory.IsValid() == FALSE || (api = apiFactory.Create(ManAPIVersion)) == NULL)
   {
      printf("Failed to create MetaTrader 4 Manager m_api interface\n");
      return;
   }
}

ServerConnect::~ServerConnect() {
   if (api != NULL)
   {
      if (api->IsConnected())
         api->Disconnect();

      api->Release();
      api = NULL;
   }
   apiFactory.WinsockCleanup();
   DeletePreviousResults();
}


bool ServerConnect::ApiValid()
{
   return (api != NULL);
}

bool ServerConnect::Connect(ServerInfo* server)
{
   int res = RET_ERROR;

   if (!ApiValid())
   {
      return false;
   }

   while (res != RET_OK)
   {
      if ((res = api->Connect(server->ip.c_str())) != RET_OK || (res = api->Login(server->login, server->pwd.c_str())) != RET_OK)
      {
         return false;
      }
   }
   return true;

}
void ServerConnect::InfoForGroupPrefix(string groupPrefix)
{
   DeletePreviousResults();
   this->groupPrefix = groupPrefix;
   SelectServerToQuery();
   if (selectedServer != NULL) {
      QueryServerForTransactionSummary(serversInfo[selectedServer]);
   }
   return;
}

void ServerConnect::DeletePreviousResults() {
   selectedServer = NULL;
   for each (auto serverInfo in serversInfo)
   {

      if (serverInfo.second->tradeResults != NULL) {
         delete(serverInfo.second->tradeResults);
      };
      delete(serverInfo.second);
   }
   serversInfo.clear();
}

void ServerConnect::SelectServerToQuery()
{
   for (int i = 0; i < serversCount; i++)
   {
      auto server = servers[i];
      if (!Connect(server)) {
         return;
      }
      serversInfo[server] = new ServerResults{ NULL,0 };
      GetUsersInValidGroups(serversInfo[server]);
   }
   
   selectedServer = NULL;
   for each (auto serverInfo in serversInfo)
   {
      if (selectedServer == NULL || serverInfo.second->usersInGroup > serversInfo[selectedServer]->usersInGroup) {
         selectedServer = serverInfo.first;
      }
   }

}

void ServerConnect::GetUsersInValidGroups(ServerResults *myServer)
{
   int usersTotal;
   UserRecord *users = api->UsersRequest(&usersTotal);
   int groupNameLen = groupPrefix.length();
   const char *groupC = groupPrefix.c_str();

   for (int i = 0; i < usersTotal; i++)
   {
      char* group = users[i].group;
      if (memequ(group, groupC, groupNameLen)) {
         myServer->usersInGroup++;
         if (myServer->groupsWithValidPrefix.find(group) == myServer->groupsWithValidPrefix.end()) {
            myServer->groupsWithValidPrefix.insert(group);
         }
      }
   }
}

void ServerConnect::QueryServerForTransactionSummary(ServerResults* selectedServer)
{
   if (selectedServer == NULL || !Connect(this->selectedServer)) {
      return;
   }
   std::vector<thread*> threads;
   for each (auto group in selectedServer->groupsWithValidPrefix)
   {
      threads.push_back(new std::thread([&]() {
         QueryServerForGroupSummary(group, selectedServer);
      }));
   }

   for each (auto t in threads)
   {
      t->join();
      delete(t);
   }
}

void ServerConnect::QueryServerForGroupSummary(string group, ServerResults* serverResult)
{
   int totalTrades = 0;
   int tradeVolume = 0;
   double tradeProfit = 0;
   int i;
   TradeRecord *trades = api->AdmTradesRequest(group.c_str(), 0, &totalTrades);
   if (totalTrades == 0) {
      return;
   }
#pragma omp parallel default(none) private(i) shared(trades, totalTrades, tradeVolume, tradeProfit)
   {
#pragma omp for reduction(+ : tradeVolume, tradeProfit)  
      for (i = 0; i < totalTrades; i++)
      {
         if (trades[i].cmd != OP_BUY && trades[i].cmd != OP_SELL) {
            continue;
         }
         tradeVolume += trades[i].volume;
         tradeProfit += trades[i].profit;
      }
   }
   mutex.lock();
   if (serverResult->tradeResults == NULL) {
      serverResult->tradeResults = new TradeResults;
   }
   serverResult->tradeResults->tradeCount += totalTrades;
   serverResult->tradeResults->tradeVolume += tradeVolume;
   serverResult->tradeResults->tradeProfit += tradeProfit;
   mutex.unlock();
}




int ServerConnect::TradeCount()
{
   if (selectedServer == NULL) {
      return 0;
   }
   return serversInfo[selectedServer]->tradeResults->tradeCount;
}

int ServerConnect::AverageVolumePerTrade()
{
   if (selectedServer == NULL) {
      return 0;
   }
   auto tradeCount = serversInfo[selectedServer]->tradeResults->tradeCount;
   auto tradeVolume = serversInfo[selectedServer]->tradeResults->tradeVolume;
   return tradeCount == 0? 0 : tradeVolume/tradeCount;
}

double ServerConnect::AverageProfitPerTrade()
{
   if (selectedServer == NULL) {
      return 0;
   }
   auto tradeCount = serversInfo[selectedServer]->tradeResults->tradeCount;
   auto tradeProfit = serversInfo[selectedServer]->tradeResults->tradeProfit;

   return tradeCount == 0 ? 0 : tradeProfit/tradeCount;
}

int ServerConnect::UsersInGroups()
{
   if (selectedServer == NULL) {
      return 0;
   }
   return serversInfo[selectedServer]->usersInGroup;
}
