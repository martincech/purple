#include "stdafx.h"
#include <functional>
#include <condition_variable>

class Dispatcher
{
public:   
   virtual void Dispatch(std::function<void(void)> &&f) = 0;
   virtual void WaitAll() = 0;
};

class SerialDispatcher : public Dispatcher
{

public:
   virtual void Dispatch(std::function<void(void)> &&f)
   {
      f();
   }
   virtual void WaitAll() {
      return;
   }
};

#include <thread>
#include <mutex>
class StdThreadDispatcher : public Dispatcher
{
   std::vector<std::thread *> threads;
public:
   virtual void Dispatch(std::function<void(void)> &&f)
   {
      std::thread *t = new std::thread(f);
      threads.push_back(t);
   }

   virtual void WaitAll() {
      for each (auto thread in threads)
      {
         thread->join();
         delete(thread);
      }
   }
};