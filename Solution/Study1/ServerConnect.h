#include "stdafx.h"
#include "MT4ManagerAPI.h"
#include "Dispatcher.h"

using namespace std;

typedef struct SServerInfo
{
   string ip;
   int login;
   string pwd;
} ServerInfo;

typedef struct STradeResults
{
   int tradeCount;
   int tradeVolume;
   double tradeProfit;
} TradeResults;

typedef struct SServerResults
{
   TradeResults *tradeResults;
   int usersInGroup;
   std::set<string> groupsWithValidPrefix;
} ServerResults;

class ServerConnect
{
private:
   CManagerFactory   apiFactory;
   CManagerInterface *api;

   ServerInfo** servers;
   ServerInfo *selectedServer;
   int serversCount;
   std::map<ServerInfo*, ServerResults*> serversInfo;
   std::mutex mutex;

   bool ApiValid();
   bool Connect(ServerInfo* server);   
   
   void GetUsersInValidGroups(ServerResults* server);
   
   void SelectServerToQuery();
   void DeletePreviousResults();
   void QueryServerForTransactionSummary(ServerResults * selectedServer);
   void QueryServerForGroupSummary(string group, ServerResults* server);
   string groupPrefix;

public:
   ServerConnect(
      ServerInfo** servers, int serversCount);
   ~ServerConnect(); 

   void InfoForGroupPrefix(string groupPrefix);

   int TradeCount();
   int AverageVolumePerTrade();
   double AverageProfitPerTrade();
   int UsersInGroups();   
};
