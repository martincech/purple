// Solution.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ServerConnect.h"

using namespace std;
template <class Function>
__int64 time_call(Function&& f)
{
   __int64 begin = GetTickCount();
   f();
   return GetTickCount() - begin;
}

__int64 GetInfoAndMeasureTime(ServerConnect *serverConnect)
{
   __int64 elapsed;
   elapsed = time_call([&]
   {
      serverConnect->InfoForGroupPrefix("demo");
   });
   return elapsed;
}

void PrintInfo(ServerConnect *serverConnect)
{
   cout << "Users in group: " << serverConnect->UsersInGroups() << endl;
   cout << "Total trades: " << serverConnect->TradeCount() << endl;
   cout << "Average volume: " << serverConnect->AverageVolumePerTrade() << endl;
   cout << "Average profit: " << serverConnect->AverageProfitPerTrade() << endl;
}



int main()
{
   const int serverCount = 3;
   ServerInfo** servers = new ServerInfo*[serverCount];
   servers[0] = new ServerInfo{"134.213.9.138", 60, "Hqy4hbR" };
   servers[1] = new ServerInfo{ "134.213.9.138", 61, "4ilbial" };
   servers[2] = new ServerInfo{ "92.52.99.147", 62, "afi3kxx" };
   
   cout << "Getting info." << endl;
   ServerConnect *serverConnect = new ServerConnect(servers, serverCount);
   auto elapsed = GetInfoAndMeasureTime(serverConnect);
   PrintInfo(serverConnect);
   cout << "Std thread version elapsed time: " << elapsed << " ms" << endl << endl;
   delete(serverConnect); 
   for (int i = 0; i < serverCount; i++)
   {
      delete(servers[i]);
   }
   delete servers;
   getchar();
   return 0;
}

