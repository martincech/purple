//+------------------------------------------------------------------+
//|                                         MetaTrader 4 Manager API |
//|                   Copyright 2001-2014, MetaQuotes Software Corp. |
//|                                        http://www.metaquotes.net |
//+------------------------------------------------------------------+
#include "stdafx.h"
#include "..\MT4ManagerAPI.h"
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CManager
  {
private:
   CManagerFactory   m_factory;
   CManagerInterface *m_manager;

public:
   CManager() : m_factory("..\\..\\mtmanapi.dll"),m_manager(NULL)
     {
      m_factory.WinsockStartup();
      if(m_factory.IsValid()==FALSE || (m_manager=m_factory.Create(ManAPIVersion))==NULL)
        {
         printf("Failed to create MetaTrader 4 Manager API interface\n");
         return;
        }
     }

   ~CManager()
     {
      if(m_manager!=NULL)
        {
         if(m_manager->IsConnected())
            m_manager->Disconnect();
         m_manager->Release();
         m_manager=NULL;
        }
      m_factory.WinsockCleanup();
     }

   bool IsValid()
     {
      return(m_manager!=NULL);
     }

   CManagerInterface* operator->()
     {
      return(m_manager);
     }
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int main(int argc, char* argv[])
  {
   int          res=RET_ERROR;
   TradeRecord *trades=NULL;
//---
   printf("MetaTrader 4 Manager API: TradeLogin\n");
   printf("Copyright 2001-2014, MetaQuotes Software Corp.\n\n");
//--- check parameters
   if(argc<5)
     {
      printf("usage: TradeLogin.exe server login password account\n");
      return(0);
     }
//--- check manager interface
   CManager manager;
   if(!manager.IsValid()) return(-1);
//--- connect
   if((res=manager->Connect(argv[1]))!=RET_OK || (res=manager->Login(atoi(argv[2]),argv[3]))!=RET_OK)
     {
      printf("Connect to %s as '%s' failed (%s)\n",argv[1],argv[2],manager->ErrorDescription(res));
      return(-1);
     }
   printf("Connect to %s as '%s' successful\n",argv[1],argv[2]);
//---
   int  login=atoi(argv[4]);
   int  total=0;
//--- get open and closed trades for the first login
   trades=manager->AdmTradesRequest(login,FALSE,&total);
   if(trades==NULL || total<1)
     {
      if(trades!=NULL) manager->MemFree(trades);
      printf("'%d' has no orders\n",login);
      return(0);
     }
   printf("'%d' has %d orders\n",login,total);
   manager->MemFree(trades);
   trades=NULL;
//---
   return(0);
  }
//+------------------------------------------------------------------+
