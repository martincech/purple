//+---------------------------------------------------------------------+
//|                                     MetaTrader 4 Manager API Sample |
//|                      Copyright 2001-2015, MetaQuotes Software Corp. |
//|                                           http://www.metaquotes.net |
//+---------------------------------------------------------------------+
#include "stdafx.h"
#include "test_c.h"
//---
using namespace std;

void Test::connect()
{
	string ip = "";
	int login = 0;
	string pw = "";

	int res = RET_ERROR;
	
	if (!m_manager.IsValid()) 
	{
		cout << "Manager interface is not valid" << endl;
		
		return;
	}

	while (res != RET_OK)
	{
		cout << "Connecting to " << ip << endl;
		
		if ((res=m_manager->Connect(ip.c_str())) != RET_OK || (res=m_manager->Login(login, pw.c_str())) != RET_OK)	
		{
			cout << "Connecting to " << ip << " as " << login << " failed: " << m_manager->ErrorDescription(res) << endl;		
		}
	}

	cout << "Connected" << endl;
	
	res = RET_ERROR;
	while (res != RET_OK)
	{
		cout << "Connecting to " << ip << endl;
		
		if ((res=m_manager_pump->Connect(ip.c_str())) != RET_OK || (res=m_manager_pump->Login(login, pw.c_str())) != RET_OK)	
		{
			cout << "Connecting to " << ip << " as " << login << " failed: " << m_manager_pump->ErrorDescription(res) << endl;		
		}
	}

	cout << "Connected as pump" << endl;

	// pumping switch ------------------------
	if (m_manager_pump->PumpingSwitchEx(pump_func, CLIENT_FLAGS_HIDETICKS | CLIENT_FLAGS_HIDEMAIL | CLIENT_FLAGS_SENDFULLNEWS | CLIENT_FLAGS_RESERVED | CLIENT_FLAGS_HIDEONLINE, this) != RET_OK)
	{
		cout << "Failed to switch to pump" << endl;
			
		m_manager->Disconnect();
		m_manager_pump->Disconnect();

		return;
	}
	
	return;
}

void __stdcall Test::pump_func(int code, int type, void * data, void *pointer)
{
	int ret = RET_ERROR;
	static int total = 0;
	Test* _this = (Test*) pointer;
	ConSymbol *cs = NULL;

	switch (code)
	{
		case PUMP_START_PUMPING:
		{
			cout << "Pumping started" << endl;

			break;
		}
		case PUMP_STOP_PUMPING:
		{			
			cout << "Pumping stopped" << endl;

			break;
		}
        case PUMP_UPDATE_TRADES:
        {
			if (data == NULL)
				break;
            
			TradeRecord* trade = (TradeRecord*)data;
              
			switch (type)
			{
                case TRANS_ADD:                                                                               
                    //A trade was added
					cout << "#" << trade->order << " was opened by " << trade->login << endl;
					break;
                case TRANS_DELETE:
                    //A trade was closed
					cout << "#" << trade->order << " was closed by " << trade->login << endl;
					break;
            }
		}
		default:
			break;
	}
}