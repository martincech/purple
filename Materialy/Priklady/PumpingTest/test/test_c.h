#include "stdafx.h"
#include "manager.h"
//---
using namespace std;
//+--------------------------------------------------------------------+
//|                                                                    |
//+--------------------------------------------------------------------+
class Test
{
private:
	CManager				m_manager, m_manager_pump;

public:
	void					connect();

private:
	static void __stdcall	pump_func(int code, int type, void *data, void *pointer);
};
//+--------------------------------------------------------------------+