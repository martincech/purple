//+------------------------------------------------------------------+
//|                                         MetaTrader 4 Manager API |
//|                   Copyright 2001-2015, MetaQuotes Software Corp. |
//|                                        http://www.metaquotes.net |
//+------------------------------------------------------------------+
#pragma once
//---
#include <iostream>
#include <windows.h>
#include <process.h>
#include <map>
#include <string>
//+------------------------------------------------------------------+