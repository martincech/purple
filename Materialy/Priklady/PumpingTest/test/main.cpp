#include "StdAfx.h"
#include "test_c.h"

//---
using namespace std;

int main(int argc, char* argv[])
{
	Test test;
	test.connect();

	//--- waiting for ESC press
	while(_getwch()!=27);

	return 0;
}
//+------------------------------------------------------------------+
